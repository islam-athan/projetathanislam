import React from 'react'
import { UseDispatch, useDispatch } from 'react-redux'
import { addEmploye } from './features/employe/employeSlice'
import { useState } from 'react'

function AddEmploye() {
    const [id,setId]=useState(0)
    const [nom,setNom]=useState("")
    const [prenom,setPrenom]=useState("")
    const [actif,setActif]=useState(false)
    const dispatch=useDispatch()
    function handleSubmit(ev){
        ev.preventDefault()
        dispatch(addEmploye({id,nom,prenom,is_actif:actif}))
    }
    
  return (
    <form onSubmit={(ev)=>handleSubmit(ev)}>AddEmploye
        <div>
            <label>id</label><input onChange={(ev)=>Number(setId(ev.target.value))} type="text"/>
        </div>
        <div>
            <label>nom</label><input  type="text" onChange={(ev)=>setNom(ev.target.value)}/>
        </div>
        <div>
            <label>prenom</label><input type="text" onChange={(ev)=>setPrenom(ev.target.value)}/>
        </div>
        <div>
            <label>id</label><input onChange={()=>setActif(!actif)} type="checkbox" checked={actif} />
        </div>
        <button  type="submit" >ajouter</button>
    </form>
  )
}

export default AddEmploye