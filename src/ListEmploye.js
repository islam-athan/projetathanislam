import React from 'react'
import { UseSelector,useDispatch, useSelector } from 'react-redux'
import { changeActif,deleteEmploye } from './features/employe/employeSlice'

function ListEmploye() {
    const employes=useSelector(state=>state.employeData.employes)
    const dispatch=useDispatch()
  return (
    <div>
        <h2>ListEmploye</h2>
<table>
<thead>
    <tr>
        <th>id</th>
        <th>nom</th>
        <th>prenom</th>
        <th>actif</th>
        <th>supprimer</th>
    </tr>
</thead>
<tbody>
    {employes.map((emp,index)=>{
        return(<tr key={index}>
         <td>{emp.id}</td>
         <td>{emp.nom}</td>
         <td>{emp.prenom}</td>
         <td><input type="checkbox" 
         checked={emp.is_actif}
         onChange={()=>dispatch(changeActif(emp.id))} /></td>
         <td>
            <button onClick={()=>dispatch(deleteEmploye(emp.id))}>supprimer</button>
         </td>
        </tr>)
    })}
</tbody>

</table>


</div>
  )
}

export default ListEmploye