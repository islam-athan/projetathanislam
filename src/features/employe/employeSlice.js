import { createSlice } from "@reduxjs/toolkit"

const initState={employes:[]}
 const employeSlice=createSlice({
name:"employes",
initialState:initState,
reducers:{
    addEmploye:(state,action)=>{
        state.employes.push(action.payload)
    },
    deleteEmploye:(state,action)=>{
        state.employes=state.employes.filter(emp=>emp.id!==action.payload)
    },
    updateEmploye:(state,action)=>{
        const index=state.employes.findIndex(emp=>emp.id===action.payload.id)
    if(index>=0){
        state.employes[index]=action.payload
    }
    },
    changeActif:(state,action)=>{
        const myEmp=state.employes.find(emp=>emp.id===action.payload)
if(myEmp){
    myEmp.is_actif=! myEmp.is_actif
}
    }
}

 })

 export const {addEmploye,updateEmploye,deleteEmploye,changeActif}=employeSlice.actions
 const employeReducer=employeSlice.reducer
 export default employeReducer