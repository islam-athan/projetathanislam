import employeReducer from "./employe/employeSlice";

import  { configureStore } from "@reduxjs/toolkit";

export const store=configureStore({
    reducer:{employeData:employeReducer}
})